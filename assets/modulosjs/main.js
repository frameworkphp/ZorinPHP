function eliminar(datos) {
    $modal = $('#modal_global');
    swal
        ({
            title: '¿Seguro que desea eliminar este registro?',
            text: "¡No podras revertir esto!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, ¡Deseo Borrarlo!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                        type: 'get',
                        dataType: 'json',
                        url: $modal.attr('data-target').replace("modal", "remove/") + datos
                    })
                    .done(function(data) {
                        console.log("success");
                        reloadData();
                        mostrar_alert(data);
                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });
            }
        });
};
var valida = 0;

function modals(datos, cargar_modal = 0, tabla = '', modulo = '', func = '') {
    $params = datos;
    var $url = '';
    $modal = '';
    if (cargar_modal == 0) {
        $modal = $('#modal_global');
        $url = $modal.attr('data-target')
    } else {
        $modal = $('#modal_secunadario');
        $url = $modal.attr('data-target');
        modulo = modulo.split(",");
        tabla = tabla.split(",");
        func = func.split(",");
        if (modulo != '') {
            $url = $url.replace(modulo[0], modulo[1]);
        }
        if (tabla != '') {
            $url = $url.replace(tabla[0], tabla[1]);
        }
        if (func != '') {
            $url = $url.replace(func[0], func[1]);
        }
    }
    $modal.html('');
    $.ajax({
            type: 'get',
            dataType: 'html',
            url: $url + '/' + $params
        })
        .done(function(response) {
            $modal.html(response);
            if (cargar_modal == 0) {
                $modal.modal('show');
            } else {
                valida = 1;
                $('#page-top').css("overflow-y", "hidden");
                $modal.css("overflow-y", "scroll");
                $('#modal_global').modal('hide');
                $modal.modal('show');
            }
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

};
$("#modal_secunadario").on('hidden.bs.modal', function() {
    valida = 0;
    $('#modal_global').modal('show');
    $('#page-top').css("overflow-y", "");
});

var formulario = $('.add');
$('.save').click(function(event) {
    event.preventDefault();
    if (valida == 1) {
        formulario = $('#modal_secunadario').children('div').children('div').children('div').children('.add');
    } else {
        formulario = $('#modal_global').children('div').children('div').children('div').children('.add');
    }
    formulario.submit();
});
formulario.submit(function(event) {
    //console.log('entro');
    event.preventDefault();
    var $form = $(this);
    if (validacion_campos()) {
        datos = "Error,Todos los campos son obligatorios,danger,top,center";
        mostrar_alert(datos);
        return false;
    }
    if (validacion_tamano_numeros()) {
        return false;
    }
    //console.log($form.attr("data-target"));
    var $formData = new FormData(this);
    $.ajax({
            method: 'post',
            dataType: 'json',
            url: $form.attr("data-target"),
            data: $formData,
            contentType: false,
            cache: false,
            processData: false,
        })
        .done(function(data) {
            mostrar_alert(data);
            if (valida == 1) {
                $('#modal_secunadario').modal('toggle');
                reloadSelect();
            } else {
                reloadData();
                $('#modal_global').modal('toggle');
            }
            // $form.children('div').children('.modal-footer').children('.save')
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

});

function mostrar_alert(datos) {
    var $datos = datos.split([',']);
    $.notify({
        title: '<strong>' + $datos[0] + '!</strong>',
        message: $datos[1] + '!'
    }, {
        type: $datos[2],
        z_index: '10000',
        placement: {
            from: $datos[3],
            align: $datos[4]
        }
    });
}

function mostrar_alert_sw(datos) {
    var $datos = datos.split([',']);
    swal({
        position: $datos[0],
        type: $datos[1],
        title: $datos[2],
        showConfirmButton: false,
        timer: $datos[3]
    });
}

function validacion_campos() {
    var $error = false;
    var validar = '';
    if (valida == 1) {
        validar = $('#modal_secunadario').children('div').children('div').children('div').children('.add input, .add select, .add textarea');
    } else {
        validar = $('.add input, .add select, .add textarea');
    }
    validar.each(function(index, el) {
        var $input = $(this);
        if ($input.attr('required')) {
            if ($input.val() === '' || $input.val() <= 0) {
                $ms = $(this).attr('data-target-ms');
                $datos = "Error," + $ms + ",danger,top,center";
                /*mostrar_alert(datos);*/
                $error = true;
                $input.addClass('invalid');
                if ($input.siblings('label.label').length == 0) {
                    $input.after('<label class="invalid label">' + $ms + '</label>');
                }
            } else {
                $input.removeClass('invalid');
                $input.siblings('label.label').remove();
            }
        }
    });
    return $error;
}

function validacion_tamano_numeros() {
    var $error = false;
    var validar = '';
    if (valida == 1) {
        validar = $('#modal_secunadario').children('div').children('div').children('div').children('.add input, .add select, .add textarea');
    } else {
        validar = $('.add input, .add select, .add textarea');
    }
    validar.each(function(index, el) {
        var $input = $(this);
        if ($input.attr('required')) {
            if (String($input.val()).length < $input.attr('tamano') && $input.is('[tamano]')) {
                $ms = $(this).attr('data-target-ms-numero');
                $datos = "Error," + $ms + ",danger,top,center";
                /*mostrar_alert(datos);*/
                $error = true;
                $input.addClass('invalid');
                if ($input.siblings('label.label').length == 0) {
                    $input.after('<label class="invalid label">' + $ms + '</label>');
                }
            } else {
                $input.removeClass('invalid');
                $input.siblings('label.label').remove();
            }
        }
    });
    return $error;
}
//validaciones generales
$('.numero').keypress(function(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8 || tecla == 0) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9-.]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
});
$(".Telefono").mask("(000) 0000-0000");
$(".Dui").mask("00000000-0");
$(".Nit").mask("0000-000000-000-0");
$('.selectpicker').selectpicker();
//fin validaciones generales

function reloadSelect() {
    var form = $('#modal_secunadario').children('div').children('div').children('div').children('.add')
    $.ajax({
        dataType: 'json',
        url: form.attr('data-target').replace('save', 'all'),
        success: function(response) {
            $tabl = form.attr('data-target').split('/');
            position = $tabl.length - 3;
            select = $("." + $tabl[position] + " select");
            select.html('');
            val = select.attr('target-val');
            val = val.split(',');
            text = select.attr('target-text');
            text = text.split(',');
            $.each(response, function(index, row) {
                // create the option
                texts = '';
                texts_valida = 1;
                if (text.length > 1) {
                    $.each(text, function(index, i) {
                        if (texts_valida == text.length) {
                            texts = texts + row[i];
                        } else {
                            texts = texts + row[i] + ' - ';
                        }
                        texts_valida = texts_valida + 1;
                    })
                } else {
                    texts = row[text[0]];
                }
                var opt = $("<option>").val(row[0]).text(texts);
                //append option to the select element
                select.append(opt);
                select.selectpicker('refresh');
            });
        }
    });
}
/* Esta función se utiliza despues de registrar   
 * se encarga de obtener los registros del base de datos y
 * actualiza el datasource del grid.
 */
function reloadData() {
    /* ".basic-info-form" contiene la ruta de donde se obtendra la información.  */
    var form = $("#modal_global");

    /* Atravéz de ajax se obtienen los resultados sin tener que recargar la página */
    $.ajax({
        dataType: 'json',
        url: form.attr('data-target').replace('modal', 'all'),
        success: function(response) {
            var grid = $('#grid').getKendoGrid();
            grid.dataSource.data(response);
            grid.refresh();
        }
    });
}