function eliminar(urll) {
	swal
	({
	  title: '¿Seguro que desea eliminar este registro?',
	  text: "¡No podras revertir esto!",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  cancelButtonText: 'Cancelar',
	  confirmButtonText: 'Si, ¡Deseo Borrarlo!'
	}).then((result) => {
		if (result.value) 
		{
			$.ajax({
				type: 'get',
				dataType: 'json',
				url: urll
			})
			.done(function(data) {
				console.log("success");
				reloadData();
				mostrar_alert(data);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}
	});			
};
function modals(datos) {
	$params = datos;
	$modal = $('#modal_global');
	$modal.html('');
	$.ajax({
		type: 'get',
		dataType: 'html',
		url: $modal.attr('data-target')+ '/'+ $params
	})
	.done(function(response) {
		$modal.html(response);
		$modal.modal('show');
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
};
$('.save').click(function(event) {
	console.log('entro');
	event.preventDefault();
	$('.add').submit();
});
$('.add').submit(function(event) {
	console.log('entro');
	event.preventDefault();
	var $form = $(this);
	if (validacion_campos()) {
		datos = "Error,Todos los campos son obligatorios,danger,top,center";
  		mostrar_alert(datos);
		return false;
	}
	//console.log($form.attr("data-target"));
	var $formData = new FormData(this);
	$.ajax({
		method: 'post', 
		dataType: 'json',
		url: $form.attr("data-target"),
		data: $formData,
	    contentType:false,
        cache:false,
        processData:false,
	})
	.done(function(data) {
		console.log("success");
		mostrar_alert(data);
		reloadData();
		$('#modal_global').modal('toggle');

	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
});
function mostrar_alert(datos) {
	var $datos = datos.split([',']);
	$.notify({
		title: '<strong>'+$datos[0]+'!</strong>',
		message: $datos[1]+'!'
	},{
		type: $datos[2],
		z_index: '10000',
		placement: {
	  		from: $datos[3],
	  		align: $datos[4]
		}
	});
}
function mostrar_alert_sw(datos) {
  var $datos = datos.split([',']);
  swal({
    position: $datos[0],
    type: $datos[1],
    title: $datos[2],
    showConfirmButton: false,
    timer: $datos[3]
  }); 
}
function validacion_campos() {
	var $error = false;
	$('.add input, .add select').each(function(index, el) {
		var $input = $(this);
		if ($input.attr('required')) {
			if ($input.val() === '' || ($input.val() <= 0 && $input.attr('name') != 'Movimiento' && $input.attr('name') != 'Naturaleza')) {
				$ms = $(this).attr('data-target-ms');
				$datos = "Error,"+$ms+",danger,top,center";
      			/*mostrar_alert(datos);*/
      			$error = true;
      			$input.addClass('invalid');
      			if ($input.siblings('label.label').length == 0) {
      				$input.after('<label class="invalid label">'+$ms+'</label>');
      			}
			}else {
				$input.removeClass('invalid');
				$input.siblings('label.label').remove();				
			}
		}
	});
	return $error;
}
/* Esta función se utiliza despues de registrar un empleado 
 * se encarga de obtener los registros del base de datos y
 * actualiza el datasource del grid.
 */
function reloadData()
{ 
	/* ".basic-info-form" contiene la ruta de donde se obtendra la información.  */
	var form = $("#modal_global");

	/* Atravéz de ajax se obtienen los resultados sin tener que recargar la página */
	$.ajax({  
	    dataType: 'json', 
	    url: form.attr('data-target').replace('modal','all'),
	    success:function(response){
	    	var grid = $('#grid').getKendoGrid();
	    	grid.dataSource.data(response);
	        grid.refresh();
	    }
	});
}