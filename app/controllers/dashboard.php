<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('employees_model', 'employees');
		$this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
	}

	public function index()
	{
		$data["js"] = array(
			"<script src='" . base_url() . "assets/plugins/charts/flot/jquery.flot.custom.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/charts/flot/jquery.flot.pie.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/charts/flot/jquery.flot.resize.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/charts/flot/jquery.flot.time.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/charts/flot/jquery.flot.growraf.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/charts/flot/jquery.flot.categories.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/charts/flot/jquery.flot.stack.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/charts/flot/jquery.flot.orderBars.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/charts/flot/jquery.flot.tooltip.min.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/charts/flot/jquery.flot.curvedLines.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/charts/sparklines/jquery.sparkline.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/charts/progressbars/progressbar.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/ui/waypoint/waypoints.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/ui/weather/skyicons.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/ui/notify/jquery.gritter.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/misc/vectormaps/jquery-jvectormap-1.2.2.min.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/misc/vectormaps/maps/jquery-jvectormap-world-mill-en.js'></script>",
	        "<script src='" . base_url() . "assets/plugins/misc/countTo/jquery.countTo.js'></script>",
			"<script src='" . base_url() . "assets/js/pages/icons.js'></script>",
			"<script src='" . base_url() . "assets/js/pages/dashboard.js'></script>");
		$data['css'] = array("<link rel='stylesheet' type='text/css' href='" . base_url() . "assets/css/sweetalert2.min.css'>",
			"<link rel='stylesheet' href='" . base_url() . "assets/css/kendo/kendo.bootstrap-v4.min.css'/>");

		$data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
		$data['title'] = "Dashboard";
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebars', $data);
		$this->load->view('dashboard', $data);
		$this->load->view('templates/footer', $data);
	}
}