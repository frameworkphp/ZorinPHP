<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class PaisesController extends Controller 
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('paises_model', 'paises');
		$this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
	}

	public function index()
	{
		$data["js"] = array(
			"<script src='" . base_url() . "assets/js/vendor/paises/loader.js'></script>",
			"<script src='" . base_url() . "assets/js/vendor/main.js'></script>");
		$data['css'] = array(	
			"<link rel='stylesheet' href='" . base_url() . "assets/css/kendo/kendo.bootstrap-v4.min.css'/>");
		$data['tabla'] = "paises";
		$data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
		$data['title'] = "Administración de Paises";
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebars', $data);
		$this->load->view('views', $data);
		$this->load->view('templates/footer', $data);
	}
	public function all()
	{
		$data = $this->paises->get_consulta();
		echo json_encode($data);
	}
	public function modal()
	{
		$Id = $this->uri->segment(3);
		if ($Id > 0 ) {
			$data['data'] = $this->paises->get_id($Id);
			$this->load->view("modals/paises",$data);
		}else{
			$this->load->view("modals/paises");
		}
	}
	public function save()
	{
		$info = array(
			':pais' => strtoupper(trim($this->input->post("pais")))
		);
		$save = $this->paises->save($info);
		if ($save != 0) {
			$datos = "Error,Ocurrio un error!,danger,top,center";
			echo json_encode($datos);
		}else{
			$datos = "success,Se agrego con exito!,success,top,center";
			echo json_encode($datos);
		}
	}
	public function update()
	{
		$info = array(
			':pais' => strtoupper(trim($this->input->post("pais"))),
			':Id_paises' => strtoupper(trim($this->input->post("Id")))
		);
		$save = $this->paises->update($info);
		if ($save != 0) {
			$datos = "Error,Ocurrio un error!,danger,top,center";
			echo json_encode($datos);
		}else{
			$datos = "success,Se edito un registro con exito!,success,top,center";
			echo json_encode($datos);
		}
	}
	public function remove()
	{
		$info = array(
			':Eliminado' => strtoupper(trim(1)),
			':Id_paises' => strtoupper(trim($this->uri->segment(3)))
		);
		$save = $this->paises->remove($info);
		if ($save != 0) {
			$datos = "Error,Ocurrio un error!,danger,top,center";
			echo json_encode($datos);
		}else{
			$datos = "success,Se elimino un registro con exito!,success,top,center";
			echo json_encode($datos);
		}
	}

}