<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu 
{
	public function render($arrMenu)
	{	$generatedMenu = "";
		foreach ($arrMenu as $item => $value) 
		{
			$icon = isset($value['icon'] ) ? $value['icon'] : '';
			if(isset($value['items']))
			{
				$generatedMenu .= "<li class=\"nav-item dropdown\">
				<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"pagesDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
		            <i class=\"".$icon."\"></i>
		            <span>".$item."</span>
		        </a>
		        <div class=\"dropdown-menu\" aria-labelledby=\"pagesDropdown\">";
				$generatedMenu .= self::renderSub($value['items']);
				$generatedMenu .= "</div></li>";
				continue;
			}
			$generatedMenu .= "<li class=\"nav-item\">
	          <a class=\"nav-link\" href=\"".site_url($value['page'])."\">
	            <i class=\"".$icon."\"></i>
	            <span>".$item."</span>
	          </a>
        	</li>";		
		}
		return $generatedMenu;
	}

	private static function renderSub($menuItems)
	{
		$generatedMenu = "";
		foreach ($menuItems as $menu => $values) 
		{
			$generatedMenu .= "<a class=\"dropdown-item\" href=\"".site_url($values)."\">".$menu."</a>";		
		}
		return $generatedMenu;
	}
}
