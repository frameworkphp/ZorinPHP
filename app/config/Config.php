<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
# URL Config

$config["base_url"] = ""; //url donde esta alojado el sitio
$config['index_page'] = "";

# Database Configuration
$config['db'] = array(
    "default" => array(
        "dbname" => "", //nombre de bd
        "username" => "", //usurio de bd
        "password" => "", // password de bd
        "port" => "3306", //port de bd de fault 3306
        "host" => ""    //host donde esta alojada la bd
        )
);?>