<div class="modal-dialog modal-dialog-centered modal-lg" >
    <div class="modal-content">
    	<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal">
		        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
		    </button>
		    <h4 class="modal-title" id="myModalLabel2">Paises</h4>
		</div>
		 <div class="modal-body">              
            <form datos="paises" method='POST' role='form' class='add' data-target="<?= $valor = (isset($data['Id_paises']))? site_url('paises/update/') : site_url('paises/save/') ?>" enctype='multipart/form-data'>
                <div class='form-group'>
                    <label>Digite un pais</label>
                    <input type='text' name='pais' class='form-control' value="<?= $valor = (isset($data['pais']))? $data['pais'] : '' ?>"  placeholder='pais' data-target-ms='El campo pais es obligatorio' required><br>
                    <input type="hidden" name="Id" value="<?= $valor = (isset($data['Id_paises']))? $data['Id_paises'] : '0' ?>">
                </div>                                          
            </form>
        </div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="button" class="btn btn-primary save">Guardar Datos</button>
		</div>
    </div>
</div>
<script src='<?=  base_url(); ?>assets/js/vendor/main.js'></script>