  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?= site_url() ?>"><i class="fas fa-home"></i></a>
      </li>
      <li class="breadcrumb-item active"><?= $title ?></li>
    </ol>
    <!-- Page Content -->
    <div class="row">
      <div class="col-md-12"><label for="" class="col-md-12"><input class="form-control search-buscar" placeholder="Buscar" type="text"></label></div>
    </div>
    <div id="grid" reload-data-target="<?= site_url((isset($modulo)) ? $modulo.'/'.$tabla.'/all' : $tabla.'/all') ?>"></div>
    <script id="template" type="text/x-kendo-template">
      <a class="btn btn-success" onclick='modals(0)' href="\#" style='color: white'> <i class='fas fa-plus-square'></i>&nbsp;&nbsp;<?= (isset($agregar)) ? $agregar : 'Nuevo Registro' ; ?></a> 

      <?= (isset($btn_ads)) ? $btn_ads : '' ; ?>
    </script>  
  </div>
  <!-- /.container-fluid -->
  <!-- Modal global -->
  <div class="modal fade arv" id="modal_global" data-target="<?= site_url((isset($modulo)) ? $modulo.'/'.$tabla.'/modal' : $tabla.'/modal'); ?>" tabindex="-1" role="dialog" aria-labelledby="modal_global" aria-hidden="true">
  </div>
  <!-- Modal secunadarios -->
  <div class="modal fade arv" id="modal_secunadario" data-target="<?= site_url((isset($modulo)) ? $modulo.'/'.$tabla.'/modal' : $tabla.'/modal'); ?>" tabindex="-1" role="dialog" aria-labelledby="modal_secunadario" aria-hidden="true">
  </div>