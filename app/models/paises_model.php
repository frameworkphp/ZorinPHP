<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class PaisesModel extends Model
{
    public function __construct()
    {
     	parent::__construct(array('brasil'));   
    }

    public function get_consulta()
    {
        $params = array(":Eliminado" => 0);
        $query = "SELECT * FROM paises WHERE Eliminado = :Eliminado";
        $result = $this->brasil->prepare($query);
        $result->execute($params);
        return $result->fetchAll();
    }
    public function get_id($Id_paises)
    {
        $params = array(":Id_paises" => $Id_paises);
        $query = "SELECT * FROM paises WHERE Id_paises = :Id_paises";
        $result = $this->brasil->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $q = $this->brasil->prepare('INSERT INTO paises(pais) VALUES(:pais)');             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    public function update($data)
    {
        try 
        {
            $q = $this->brasil->prepare('UPDATE paises SET pais = :pais WHERE Id_paises = :Id_paises');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    public function remove($data)
    {
        try 
        {
            $q = $this->brasil->prepare('UPDATE paises SET Eliminado = :Eliminado WHERE Id_paises= :Id_paises');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}